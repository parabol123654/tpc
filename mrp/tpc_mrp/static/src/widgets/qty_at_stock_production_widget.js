/** @odoo-module **/

import { registry } from "@web/core/registry";
import { useService } from "@web/core/utils/hooks";
import { usePopover } from "@web/core/popover/popover_hook";
import { Component, onWillRender } from "@odoo/owl";

export class QtyAtDatePopover extends Component {
    setup() {
        this.actionService = useService("action");
    }
}

QtyAtDatePopover.template = "tpc_mrp.QtyAtDatePopover";

export class QtyAtDateWidget extends Component {
    setup() {
        this.popover = usePopover(this.constructor.components.Popover, { position: "top" });
        this.calcData = {};
        onWillRender(() => {
            this.initCalcData();
        })
    }

    initCalcData() {
        // calculate data not in record
        const { data } = this.props.record;
        this.calcData.will_be_fulfilled = data.product_qty_available >= data.product_uom_qty;
        this.calcData.forecasted_issue = !this.calcData.will_be_fulfilled;
    }

    showPopup(ev) {
        this.popover.open(ev.currentTarget, {
            record: this.props.record,
            calcData: this.calcData,
        });
    }
}

QtyAtDateWidget.components = { Popover: QtyAtDatePopover };
QtyAtDateWidget.template = "tpc_mrp.QtyAtDate";

export const qtyAtDateWidget = {
    component: QtyAtDateWidget,
};
registry.category("view_widgets").add("qty_at_stock_production_widget", qtyAtDateWidget);
