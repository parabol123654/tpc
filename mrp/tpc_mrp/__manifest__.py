# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'TPC MRP',
    'version': '1.0.1',
    'category': 'Accounting',
    'summary': 'TPC MRP',
    'description': 'TPC MRP',
    'sequence': '10',
    'website': '',
    'author': '',
    'depends': [
        'mrp',
    ],
    'data': [
        'wizard/mrp_consumption_warning_views.xml',
        'views/mrp_production.xml',
    ],
    'installable': True,
    'auto_install': True,
    'assets': {
        'web.assets_backend': [
            'tpc_mrp/static/src/**/*',
        ],
    },
    'license': 'LGPL-3',
}

