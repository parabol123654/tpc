# -*- coding: utf-8 -*-

import json
import datetime
import math
import re

from ast import literal_eval
from collections import defaultdict
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _, Command
from odoo.addons.web.controllers.utils import clean_action
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_compare, float_round, float_is_zero, format_datetime
from odoo.tools.misc import OrderedSet, format_date, groupby as tools_groupby


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            vals['state'] = 'confirmed'
        return super().create(vals_list)
    
    def _get_consumption_issues(self):
        """Compare the quantity consumed of the components, the expected quantity
        on the BoM and the consumption parameter on the order.

        :return: list of tuples (order_id, product_id, consumed_qty, expected_qty) where the
            consumption isn't honored. order_id and product_id are recordset of mrp.production
            and product.product respectively
        :rtype: list
        """
        issues = []
        for order in self:
            for move in order.move_raw_ids.filtered(lambda m: m.product_id.detailed_type == 'product'):
                available = sum([
                    ml.product_uom_id._compute_quantity(ml.quantity, move.product_uom, round=False)
                    for ml in move.move_line_ids
                ])
                
                if float_compare(available, move.product_uom_qty, precision_rounding=move.product_uom.rounding) < 0:
                    issues.append((order, move.product_id, available, move.product_uom_qty))

        return issues
    
    def _action_generate_consumption_wizard(self, consumption_issues):
        # ctx = self.env.context.copy()
        # lines = []
        # for order, product_id, consumed_qty, expected_qty in consumption_issues:
        #     lines.append((0, 0, {
        #         'mrp_production_id': order.id,
        #         'product_id': product_id.id,
        #         'consumption': order.consumption,
        #         'product_uom_id': product_id.uom_id.id,
        #         'product_consumed_qty_uom': consumed_qty,
        #         'product_expected_qty_uom': expected_qty
        #     }))
        # ctx.update({'default_mrp_production_ids': self.ids,
        #             'default_mrp_consumption_warning_line_ids': lines,
        #             'form_view_ref': False})
        # action = self.env["ir.actions.actions"]._for_xml_id("mrp.action_mrp_consumption_warning")
        # action['context'] = ctx
        raise ValidationError("Component not eaui %s", consumption_issues[0][1].display_name)
    
    def _action_generate_consumption_wizard(self, consumption_issues):
        ctx = self.env.context.copy()
        lines = []
        for order, product_id, consumed_qty, expected_qty in consumption_issues:
            lines.append((0, 0, {
                'mrp_production_id': order.id,
                'product_id': product_id.id,
                'consumption': order.consumption,
                'product_uom_id': product_id.uom_id.id,
                'product_consumed_qty_uom': consumed_qty,
                'product_expected_qty_uom': expected_qty
            }))
        ctx.update({'default_mrp_production_ids': self.ids,
                    'default_mrp_consumption_warning_line_ids': lines,
                    'form_view_ref': False})
        action = self.env["ir.actions.actions"]._for_xml_id("tpc_mrp.action_mrp_consumption_warning")
        action['context'] = ctx
        return action

    @api.depends('company_id', 'bom_id', 'product_id', 'product_qty', 'product_uom_id', 'location_src_id')
    def _compute_move_raw_ids(self):
        for production in self:
            if production.state != 'draft' or self.env.context.get('skip_compute_move_raw_ids'):
                continue
            list_move_raw = [Command.link(move.id) for move in production.move_raw_ids.filtered(lambda m: not m.bom_line_id)]
            if not production.bom_id and not production._origin.product_id:
                production.move_raw_ids = list_move_raw
            if any(move.bom_line_id.bom_id != production.bom_id or move.bom_line_id._skip_bom_line(production.product_id)\
                for move in production.move_raw_ids if move.bom_line_id):
                production.move_raw_ids = [Command.clear()]
            if production.bom_id and production.product_id and production.product_qty > 0:
                # keep manual entries
                moves_raw_values = production._get_moves_raw_values()
                move_raw_dict = {move.bom_line_id.id: move for move in production.move_raw_ids.filtered(lambda m: m.bom_line_id)}
                for move_raw_values in moves_raw_values:
                    if move_raw_values['bom_line_id'] in move_raw_dict:
                        continue
                    # add new entries
                    list_move_raw += [Command.create(move_raw_values)]
                production.move_raw_ids = list_move_raw
            else:
                production.move_raw_ids = [Command.delete(move.id) for move in production.move_raw_ids.filtered(lambda m: m.bom_line_id)]
