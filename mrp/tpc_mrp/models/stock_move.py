# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.tools import float_compare, float_round, float_is_zero, OrderedSet

class StockMove(models.Model):
    _inherit = 'stock.move'

    standard_price = fields.Float('Cost', company_dependent=True, store=True,
        digits='Product Price', groups="base.group_user", related='product_id.standard_price')
    cost_currency_id = fields.Many2one('res.currency', 'Cost Currency', 
        compute='_compute_cost_currency_id')
    amount_total = fields.Monetary(string="Total", store=True, compute='_compute_amount_total', currency_field='cost_currency_id',)
    display_qty_widget = fields.Boolean(compute='_compute_display_qty_widget')

    @api.depends('product_id', 'product_uom_qty', 'quantity', 'standard_price')
    def _compute_amount_total(self):
        for move in self: 
            move.amount_total = move.product_uom_qty * move.standard_price
                
    @api.depends('company_id')
    @api.depends_context('company')
    def _compute_cost_currency_id(self):
        env_currency_id = self.env.company.currency_id.id
        for move in self:
            move.cost_currency_id = move.company_id.currency_id.id or env_currency_id
            
    @api.depends('product_id.detailed_type', 'product_id')
    def _compute_display_qty_widget(self):
        """Compute the visibility of the inventory widget."""
        for move in self:
            display_qty_widget = False
            if move.product_id.detailed_type == 'product':
                display_qty_widget = True
            
            move.display_qty_widget = display_qty_widget
            
    def _should_bypass_set_qty_producing(self):
        return True
