# -*- coding: utf-8 -*-
{
    'name': 'TPC Sale',
    'version': '1.0.1',
    'category': 'Accounting',
    'summary': 'TPC Sale',
    'description': 'TPC Sale',
    'sequence': '10',
    'website': '',
    'author': '',
    'license': 'LGPL-3',
    'depends': ['sale_stock'],
    'data': [
        'views/sale_order.xml',
        'views/res_config_settings_views.xml',
    ],
    'application': False,
}

