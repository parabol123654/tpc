# -*- coding: utf-8 -*-

from odoo import fields, models, _
from odoo.exceptions import UserError
from odoo.addons.sale.models.sale_order import SALE_ORDER_STATE

SALE_ORDER_STATE = [
    ('draft', "Quotation"),
    ('sent', "Confirm"),
    ('sale', "Sales Order"),
    ('cancel', "Cancelled"),
]

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def _confirm_allowed(self):
        """Returns whether the order to be approved by the current user"""
        self.so_manager_approval = (
            not self.company_id.so_order_approval
            or (self.company_id.so_order_approval
                and not any(
                    (l.amount_commission / l.price_total) * 100 >= self.company_id.so_double_validation_number
                    for l in self.order_line.filtered(lambda l: not l.display_type and l.price_total > 0)
                ))
            or self.user_has_groups('sales_team.group_sale_manager'))
    
    so_manager_approval = fields.Boolean(string="Order Manager Approval", compute=_confirm_allowed)
    
    # def action_confirm(self):
    #     return super(SaleOrder, self).action_confirm()
    
    def action_send(self):
        if not all(order.so_manager_approval for order in self):
            raise UserError(
                "Đơn hàng có hoa hồng cao hơn so với giới hạn khống chế. Cần quản lý xác nhận."
            )
        self.write(self._prepare_validation_values())
        return True
        
    def _prepare_validation_values(self):
        """ Prepare the sales order confirmation values.

        Note: self can contain multiple records.

        :return: Sales Order confirmation values
        :rtype: dict
        """
        return {
            'state': 'sent',
            'date_order': fields.Datetime.now()
        }

    def _prepare_confirmation_values(self):
        """ Prepare the sales order confirmation values.

        Note: self can contain multiple records.

        :return: Sales Order confirmation values
        :rtype: dict
        """
        return {'state': 'sale'}
