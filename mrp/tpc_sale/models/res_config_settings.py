# -*- coding: utf-8 -*-

from odoo import api, fields, models

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    so_order_approval = fields.Boolean(
        related='company_id.so_order_approval', 
        string="Sale Order Approval", readonly=False)
    so_double_validation_number = fields.Float(
        related='company_id.so_double_validation_number', 
        string="Minimum Percent (%)", readonly=False)

    @api.onchange('so_double_validation_number')
    def _onchange_so_double_validation_number(self):
        if self.so_double_validation_number < 0:
            self.so_double_validation_number = 1
        elif self.so_double_validation_number > 100:
            self.so_double_validation_number = 1
