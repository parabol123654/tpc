# -*- coding: utf-8 -*-

from odoo import fields, models

class Company(models.Model):
    _inherit = 'res.company'
    
    so_order_approval = fields.Boolean(string="Sale Order Approval", readonly=False)
    so_double_validation_number = fields.Float(string="Minimum Percent (%)", readonly=False)
