# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    product_cost = fields.Monetary(string='Product Cost')
    amount_commission = fields.Monetary(string="Commission Amount")
    delivery_fee = fields.Monetary(string="Delivery Fee")
    amount_profit = fields.Monetary(string="Profit Amount")
    
    @api.onchange('product_id', 'product_id.standard_price', 'amount_commission', 'product_uom', 
                  'delivery_fee', 'product_uom_qty', 'currency_id', 'price_unit', 'tax_id')
    def _on_change_line_cost(self):
        if not self.product_id:
            return {}

        # Compute based on pricetype
        amount_unit = self.product_id._price_compute('standard_price', uom=self.product_uom)[self.product_id.id]
        amount = amount_unit * self.product_uom_qty or 0.0 
        self.product_cost = (self.currency_id.round(amount) if self.currency_id else round(amount, 2))

        # Lợi nhuận = (Giá bán - giá vốn - chi phí vận chuyển - hoa hồng ....) x 0.8
        # Lợi nhuận phải tính dựa trên giá chưa có thuế VAT
        self.amount_profit = (max([self.price_total - self.product_cost \
            - self.delivery_fee - self.amount_commission, 0])) * 0.8
